
var React = require('react');
var TinyMceEditor = require('../component.react');


var MyTest = React.createClass({
    onChange(event) {
        console.log(event.target.getContent());
    },

    tinymceConfig: {
        toolbar: "undo redo | fontselect | fontsizeselect | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        menubar : false
    },

    render: function() {
        return (
            <TinyMceEditor config={this.tinymceConfig} onChange={this.onChange} onRedo={this.onChange} onUndo={this.onChange} content="ssss"/>
        )
    }
});

React.render(<MyTest/>, document.getElementById('app'));